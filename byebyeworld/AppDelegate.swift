//
//  AppDelegate.swift
//  byebyeworld
//
//  Created by FL CDPS Support on 2015-10-29.
//  Copyright © 2015 support. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

